<?php

use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\PhoneAuthController;
use App\Http\Controllers\RoleAndPermissionController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/admin', [LoginController::class, 'showAdminLoginForm'])->name('admin.login-view');
Route::get('/admin', [LoginController::class, 'showAdminLoginForm'])->name('admin.login-view');

Route::post('/logout', [LoginController::class, 'showAdminLoginForm'])->name('logout');

Route::get('/admin/register', [RegisterController::class, 'showAdminRegisterForm'])->name('admin.register-view');
Route::post('/admin/register', [RegisterController::class, 'createAdmin'])->name('admin.register');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/admin/dashboard', function () {
    return view('admin');
})->middleware('auth:admin');
Auth::routes();

Route::post('/save-token', [App\Http\Controllers\HomeController::class, 'saveToken'])->name('save-token');
Route::post('/send-notification', [App\Http\Controllers\HomeController::class, 'sendNotification'])->name('send.notification');



Route::get('/phone-auth', [PhoneAuthController::class, 'index']);



Route::get('/articles', [ArticlesController::class, 'index']);

Route::get('/articles/withoutcache', [ArticlesController::class, 'allWithoutcache']);



Route::get('send/email', function () {

    $send_mail = 'vuchidoanh2003@gmail.com';

    dispatch(new App\Jobs\SendEmailQueueJob($send_mail));

    dd('send mail successfully !!');
});

Route::get('/create-role', [RoleAndPermissionController::class, 'createRole']);

Route::get('/create-permission', [RoleAndPermissionController::class, 'createPermission']);

Route::get('/assign', [RoleAndPermissionController::class, 'assignRoleForPermission']);

Route::get('/deleteP', [RoleAndPermissionController::class, 'deleteP']);
