<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('copouns', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->string('start_date');
            $table->string('end_date');
            $table->bigInteger('tour_id');
            $table->bigInteger('cate_id');
            $table->bigInteger('percentage');
            $table->bigInteger('fixed');
            $table->bigInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('copouns');
    }
};
