<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Http\Request;

class RoleAndPermissionController extends Controller
{
    public function createRole (Request $request){
        $role = Role::create(['name' => 
        // $request->role 
        'writer2'
    ]);
    }
    public function createPermission (Request $request){
        $permission = Permission::create(['name' => 
        // $request->permission
        'edit2'
    ]);
        
    }
    public function assignRoleForPermission(Request $request){
        $permission = Permission::find($request->permission_id);
        $permission2 = Permission::find($request->permission_id2);
        $role = Role::find($request->role_id);

        $role->syncPermissions($permission, $permission2);
    }
    public function deleteP(Request $request){
        $permission = Permission::find($request->permission_id);
        $role = Role::find($request->role_id);

        $role->revokePermissionTo($permission);
    }
}
