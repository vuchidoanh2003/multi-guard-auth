<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('start_date');
            $table->string('end_date');
            $table->bigInteger('price');
            $table->bigInteger('sale_percentage');
            $table->string('duration');
            $table->string('start_destination');
            $table->string('end_destination');
            $table->string('number_of_tourists');
            $table->string('details');
            $table->foreignId('Main_img_id');
            $table->tinyInteger('status');
            $table->bigInteger('view_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tours');
    }
};
