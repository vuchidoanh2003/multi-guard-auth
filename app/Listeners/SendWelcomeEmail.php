<?php

namespace App\Listeners;

use App\Events\RegisterEvent;
use App\Mail\SendEmailQueueDemo;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     */
    public function handle(RegisterEvent $event): void
    {
        Mail::to($event->user->email)->send(new SendEmailQueueDemo($event->user));
    }
}
